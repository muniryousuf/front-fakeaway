<?php

namespace App\Data\Repositories;

use App\Data\Models\Deal;
use App\Data\Models\MealDealDays;
use App\Data\Models\MealPlan;
use App\Data\Models\Products;
use function App\Helpers\paginator;

class MealPlanRepository
{
    protected $model;

    public function __construct(MealPlan $model) {
        $this->model = $model;
    }

    /**
     * @param bool $pagination
     * @param int $perPage
     * @param array $input
     * @return array|mixed
     */
    public function findByAll($pagination = false,$perPage = 10, $input = [])
    {
        $data = array();

        $data['data'] = MealDealDays::where('id_deal', 1)->get();

        return $data;
    }

    /**
     * @param $id
     * @return array|null
     */
    public function findById($id)
    {
        $data = array();
        $query = $this->model->find($id);

        if ($query != NULL) {
            $data = $query;
        } else {
            $data = null;
        }

        return $data;
    }

    /**
     * @param $request
     * @param $id
     * @return mixed
     */
    public function updateRecord($request, $id)
    {
        $data = $this->model->findOrFail($id);
        $data->fill($request)->save();
        return $data;
    }
}
