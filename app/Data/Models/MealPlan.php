<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class MealPlan extends Model
{
    protected $table = 'meal_plans';

    protected $with = ['deals'];

    public function deals()
    {
        return $this->hasMany(MealDeal::class,'id_plan','id');
    }
}
