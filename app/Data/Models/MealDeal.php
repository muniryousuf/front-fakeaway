<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class MealDeal extends Model
{
    protected $table = 'meal_deals';

    protected $with = ['meals'];

    public function meals()
    {
        return $this->hasMany(MealDealDays::class,'id_deal','id');
    }
}
