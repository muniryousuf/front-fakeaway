<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class MealDealDays extends Model
{
    protected $table = 'meal_deal_days';
}
