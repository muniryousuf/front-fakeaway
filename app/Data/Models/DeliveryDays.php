<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryDays extends Model
{
    protected $table = 'delivery_days';
}
