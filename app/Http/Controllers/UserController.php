<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use App\Mail\UserRegister;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{

    public function register(Request $request)
    {

        $validator = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string'],
        ]);

        $requestData = $request->all();

        if(!isset($requestData['gymID']) || $requestData['gymID'] === 'undefined'){
            $requestData['gymID']= 0;
        }


        $userId = User::insertGetId([
            'name' => $requestData['name'],
            'email' => $requestData['email'],
            'phone_number' => $requestData['phone'],
            'password' => Hash::make($requestData['password']),
            'gym_id' => $requestData['gymID'],
        ]);
        Mail::to($requestData['email'])->send(new UserRegister($requestData));
        $user = User::find($userId);
        $user->access_token = $user->createToken('myApp')->accessToken;
        $output = ['access_token' => $user->access_token, 'data' => $user, 'message' => 'Success'];
        return response()->json($output, Response::HTTP_OK);
    }


}
