<?php

namespace App\Http\Controllers\Api;

use App\Data\Models\DeliveryDays;
use App\Data\Repositories\DeliveryChargesRepository;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;
use Symfony\Component\HttpFoundation\Response;

class DeliveryChargesController extends Controller
{
    protected $_repository;

    public function __construct(DeliveryChargesRepository $repository)
    {
        $this->_repository = $repository;
    }

    public function checkPostalCode(Request $request)
    {
        $requestData = $request->all();

        $validator =  Validator::make($requestData, [
            'postal_code' => 'required'
        ]);

        if ($validator->fails()) {
            $code = 401;
            $output = ['error' => ['code' => $code, 'message' => $validator->errors()->first()]];
            return response()->json($output, $code);
        }

        $data = $this->_repository->checkPostalCode($requestData);

        if ($data) {

            $output = ['data' => $data, 'message' => "great we deliver to your area"];
            return response()->json($output, Response::HTTP_OK);

        } else {

            $code = 401;
            $output = ['error' => ['code' => $code, 'message' => "sorry we don’t deliver to you yet, but we will in a 2 weeks"]];
            return response()->json($output, $code);

        }
    }

    public function availableDays() {

        $data = [];

        $days = DeliveryDays::all()->toArray();

        foreach ($days as $d) {

            $nextWeekDay = new Carbon('next ' . $d['day']);
            $tomorrow = new Carbon('tomorrow');


            if ($tomorrow == $nextWeekDay) {

                $data[] = array("day" => $d['day'], "date" => $tomorrow->addDays(7)->format('Y-m-d'));

            } else {

                $data[] = array("day" => $d['day'], "date" => $nextWeekDay->format('Y-m-d'));

            }
        }

        $output = ['data' => $data, 'message' => "delivery days"];
        return response()->json($output, Response::HTTP_OK);

    }
}
