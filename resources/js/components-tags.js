// auth components
Vue.component(
	'header-menu',
	require('./components/common-components/nav-menu.vue').default
)
Vue.component(
	'slider',
	require('./components/common-components/slider.vue').default
)
Vue.component(
	'footer-menu',
	require('./components/common-components/footer.vue').default
)
Vue.component(
	'alert',
	require('./components/common-components/Alert.vue').default
)

Vue.component(
	'add-product',
	require('./components/order-page/popup/add-product.vue').default
)
Vue.component(
	'edit-product',
	require('./components/order-page/popup/edit-product.vue').default
)

Vue.component(
	'download-menu',
	require('./components/common-components/download-menu.vue').default
)
Vue.component(
	'postal-code-popup',
	require('./components/common-components/postal-code-popup.vue').default
)

Vue.component(
	'add-deal',
	require('./components/order-page/popup/add-deal.vue').default
)

Vue.component(
	'edit-deal',
	require('./components/order-page/popup/edit-deal.vue').default
)

Vue.component(
	'testimonials',
	require('./components/common-components/testimonials.vue').default
)

Vue.component(
	'view-detail',
	require('./components/popup/ViewDetail.vue').default
)

Vue.component(
	'post-code-popup',
	require('./components/popup/PostCodePopup.vue').default
)
Vue.component(
	'special-offer',
	require('./components/popup/SpecialOffer.vue').default
)
Vue.component('meal-plan', require('./components/popup/MealPlan.vue').default)
