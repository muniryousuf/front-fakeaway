import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

let cart = window.localStorage.getItem('cart');
let cartCount = window.localStorage.getItem('cartCount');
let deal = window.localStorage.getItem('selected_deal');
let postal_code = window.localStorage.getItem('postal_code');
let gymID = window.localStorage.getItem('gymID');
let specialPopup = window.localStorage.getItem('specialPopup');
let deliveryCharges = window.localStorage.getItem('delivery_charges');

export default new Vuex.Store({

    // You can use it as state property
    state: {
        cartArray:[{}],
        delivery_charges: deliveryCharges ? deliveryCharges: '',
        postal_code: postal_code ? postal_code : '',
        cart: cart ? JSON.parse(cart) : [],
        selected_deal: deal ? JSON.parse(deal) : [],
        cartCount: cartCount ? parseInt(cartCount) : 0,
        gymID: gymID ? gymID: '',
        specialPopup: specialPopup ? specialPopup: 0,
        user_data: localStorage.getItem('user_data') ? localStorage.getItem('user_data') : '',
    },

    // You can use it as a state getter function (probably the best solution)
    getters: {
        getAllCartArray(state){
            return  state.cartArray;
        },
        getDeliveryCharges(state){
            return state.delivery_charges;
        },
        getPostalCode(state){
            return state.postal_code;
        },
        getCartCount(state){
            return state.cartCount;
        },
        getCart(state){
            return state.cart;
        },

        getSelectedDeal(state){
            return state.selected_deal;
        },
        getSpecialPopup(state){
            return  state.specialPopup;
        },
        getGymID(state){
            return  state.gymID;
        },
        getUserData(state){
            if (state.user_data == "" || state.user_data == null) {
                return localStorage.getItem('user_data');
            } else {
                return state.user_data;
            }
        }
    },

    // Mutation for when you use it as state property
    mutations: {
        setAllCartArray(state, data){
            state.cartArray = data;
        },
        setCart(state, data){
            state.cart = data;
        },
        setDeliveryCharges(state,data){
            state.delivery_charges = data
            window.localStorage.setItem('delivery_charges', data);
        },
        setPostalCode(state, data){

            state.postal_code = data

            window.localStorage.setItem('postal_code', data);
        },
        addToCart(state, item) {

            let found = state.cart.find(product => product.id == item.id);

            if (found) {
                found.quantity ++;
                found.totalPrice = found.quantity * found.price;
            } else {
                state.cart.push({'id': item.id, 'name': item.name, 'price': item.price, 'quantity': 1, 'totalPrice':item.price});
                Vue.set(item, 'quantity', 1);
                Vue.set(item, 'totalPrice', item.price);
            }

            state.cartCount++;
            this.commit('saveCart');

        },

        addDealToCart(state, item) {

            let found = state.cart.find(product => product.id == item.id);

            if (found) {

                found.totalPrice = item.price;
                found.price = item.price;
                found.items = item.products;
                found.meal_type = item.meal_type,
                found.name = item.name

            } else {
                state.cart.push({'id': item.id, 'name': item.name, 'price': item.price, 'quantity': 1, 'totalPrice':item.price, 'items': item.products, 'meal_type' : item.meal_type});
                Vue.set(item, 'quantity', 1);
                Vue.set(item, 'totalPrice', item.price);

                state.cartCount++;
            }

            this.commit('saveCart');

        },

        removeFromCart(state, item) {
            let index = state.cart.indexOf(item);

            if (index > -1) {
                let product = state.cart[index];
                state.cartCount -= product.quantity;

                state.cart.splice(index, 1);
            }
            this.commit('saveCart');
        },

        saveCart(state) {
            window.localStorage.setItem('cart', JSON.stringify(state.cart));
            window.localStorage.setItem('cartCount', state.cartCount);
        },

        setSelectedDeal(state, data){
            state.selected_deal = data;
            window.localStorage.setItem('selected_deal', JSON.stringify(state.selected_deal));
        },

        setGymID(state, data){
            console.log(data);
            window.localStorage.setItem('gymID',data);
        },

        setSpecialPopup(state, data){
            console.log(data);
            window.localStorage.setItem('specialPopup', data);
        },

        setUserData(state,data){
            localStorage.setItem('user_data',data);
            state.order_type = localStorage.getItem('user_data');
        }
    },
});
