require('./bootstrap')
window.Vue = require('vue')

import Vue from 'vue'
import VueRouter from 'vue-router'
import Vuex from 'vuex'
import store from './store.js'
import VueCtkDateTimePicker from 'vue-ctk-date-time-picker'
import 'vue-ctk-date-time-picker/dist/vue-ctk-date-time-picker.css'
import Lingallery from 'lingallery'

Vue.use(VueRouter)

import BootstrapVue from 'bootstrap-vue'
import { ModalPlugin } from 'bootstrap-vue'

import VeeValidate from 'vee-validate'
const config = {
    errorBagName: 'errorBag',
    events: 'input',
}
Vue.use(VeeValidate, config)

Vue.use(ModalPlugin)
Vue.use(BootstrapVue)

Vue.use(Vuex)
Vue.component('VueCtkDateTimePicker', VueCtkDateTimePicker)
Vue.component('lingallery', Lingallery)

require('./components-tags')
const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: require('./components/Home.vue').default,
        },
        {
            path: '/reservation',
            name: 'reservation',
            component: require('./components/Reservation.vue').default,
        },
        {
            path: '/products',
            name: 'products',
            component: require('./components/Products.vue').default,
        },
        {
            path: '/our-plans',
            name: 'ourplans',
            component: require('./components/OurPlans.vue').default,
        },
        {
            path: '/edit-plans',
            name: 'edit-plans',
            component: require('./components/EditPlans.vue').default,
        },
        {
            path: '/check-out',
            name: 'check-out',
            component: require('./components/Checkout.vue').default,
        },
        {
            path: '/faq',
            name: 'faq',
            component: require('./components/Faq.vue').default,
        },
        {
            path: '/thankyou',
            name: 'thankyou',
            component: require('./components/Thankyou.vue').default,
        },

        {
            path: '/contact-us',
            name: 'contact-us',
            component: require('./components/Contactus.vue').default,
        },
        {
            path: '/online-order/:id',
            name: 'online-order',
            component: require('./components/order-page/main.vue').default,
        },
        {
            path: '/login',
            name: 'login',
            component: require('./components/Login.vue').default,
        },
        {
            path: '/register',
            name: 'register',
            component: require('./components/Register.vue').default,
        },
        {
            path: '/blog',
            name: 'blog',
            component: require('./components/Blog.vue').default,
        },
        {
            path: '/how-it-works',
            name: 'how-it-works',
            component: require('./components/HowItWorks.vue').default,
        },
        {
            path: '/privacy-policy',
            name: 'privacy-policy',
            component: require('./components/PrivacyPolicy.vue').default,
        },
        {
            path: '/cookie-policy',
            name: 'cookie-policy',
            component: require('./components/CookiePolicy.vue').default,
        },
        {
            path: '/terms',
            name: 'terms',
            component: require('./components/Terms.vue').default,
        },
        {
            path: '/gallery',
            name: 'gallery',
            component: require('./components/Gallery.vue').default,
        },
        {
            path: '/become-a-partner',
            name: 'become-a-partner',
            component: require('./components/BecomePartner.vue').default,
        },
        {
            path: '/testimonial',
            name: 'testimonial',
            component: require('./components/Testimonial.vue').default,
        },
        {
            name: 'password-reset',
            path: '/password/reset/:token/:email',
            component: require('./components/ResetPassword.vue').default,
        },
        {
            path: '/forgot-password',
            name: 'forgot-password',
            component: require('./components/ForgotPassowrd.vue').default,
        },
    ],
})

new Vue({
    el: '#app',
    components: {},
    router,
    store,
    data() {
        return {
            loading: true,
        }
    },
    created() {
        setTimeout(() => {
            this.loading = false
        }, 1000)
    },
})
